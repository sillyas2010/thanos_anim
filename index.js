const canvasImageScale = 50,
    animateTimeMS = 7000,
    canvasCount = 35;

$.fn.visible = function() {
    return this.css('visibility', 'visible');
};

$.fn.invisible = function() {
    return this.css('visibility', 'hidden');
};

$(document).ready(() => {
    // canvas
    let canvas = document.getElementById('myCanvas'),
        ctx = canvas.getContext("2d"),
        fileinput = document.getElementById('file-1'),
        imageData = null, // input file
        img = new Image(),
        interval = null,
        intervalFunc = () => {};

    fileinput.onchange = (evt) => {
        let files = evt.target.files, // FileList object
            file = files[0];

        if (file && file.type.match('image.*')) {
            clearInterval(interval);
            interval = null;
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            let reader = new FileReader();
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);
            reader.onload = function(evt) {
                if (evt.target.readyState == FileReader.DONE) {
                    img.src = evt.target.result;
                    intervalFunc = () => {
                        fitImageOn(canvas, img, canvasImageScale);

                        imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                    };
                    img.onload = () => {
                        $('.animate-trg').attr('disabled', false);
                        interval = setInterval(intervalFunc, 200)
                    };
                }
            }
        } else {
            $('.animate-trg').attr('disabled', 'disabled');
            alert("not an image");
        }
    };

    $('.animate-trg').on('click', () => {
        fileinput.disabled = true;
        clearInterval(interval);
        interval = null;

        animationStart(canvas, imageData);

        setTimeout(() => {
            fileinput.disabled = false;
            interval = setInterval(intervalFunc, 200);
            // $('#copyCanvas-content').html('');
            $('#myCanvas').visible();
            $('#copyCanvas').invisible();
        }, animateTimeMS);
    });
});

function fitImageOn(canvas, imageObj, scaleMultiplier) {
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;

    let imageAspectRatio = imageObj.width / imageObj.height,
        canvasAspectRatio = canvas.width / canvas.height,
        ctx = canvas.getContext("2d"),
        renderableHeight, renderableWidth, xStart, yStart;

    if (scaleMultiplier > 1) {
        scaleMultiplier /= 100;
    } else {
        scaleMultiplier = scaleMultiplier || 1;
    }

    // If image's aspect ratio is less than canvas's we fit on height
    if (imageAspectRatio < canvasAspectRatio) {
        renderableHeight = canvas.height;
        renderableWidth = imageObj.width * (renderableHeight / imageObj.height);
    }

    // If image's aspect ratio is greater than canvas's we fit on width
    else if (imageAspectRatio > canvasAspectRatio) {
        renderableWidth = canvas.width;
        renderableHeight = imageObj.height * (renderableWidth / imageObj.width);
    }

    // Happy path - keep aspect ratio
    else {
        renderableHeight = canvas.height;
        renderableWidth = canvas.width;
    }

    xStart = (canvas.width - (renderableWidth * scaleMultiplier)) / 2;
    yStart = (canvas.height - (renderableHeight * scaleMultiplier)) / 2;

    ctx.drawImage(imageObj, xStart, yStart, renderableWidth * scaleMultiplier, renderableHeight * scaleMultiplier);
};

function animationStart(canvas, imageData) {
    let pixelArr = imageData.data,
        imageDataArray = [];

    $("#copyCanvas-content").html('');

    createBlankImageData(imageData, imageDataArray);
    //put pixel info to imageDataArray (Weighted Distributed)
    for (let i = 0; i < pixelArr.length; i += 4) {
        //find the highest probability canvas the pixel should be in
        let p = Math.floor((i / pixelArr.length) * canvasCount);
        let a = imageDataArray[weightedRandomDistrib(p)];
        a[i] = pixelArr[i];
        a[i + 1] = pixelArr[i + 1];
        a[i + 2] = pixelArr[i + 2];
        a[i + 3] = pixelArr[i + 3];
    }

    //create canvas for each imageData and append to target element
    for (let i = 0; i < canvasCount; i++) {
        let c = newCanvasFromImageData(imageDataArray[i], canvas.width, canvas.height);
        c.classList.add("dust");
        $("#copyCanvas-content").append(c);
    }
    $('#myCanvas').invisible();
    $('#copyCanvas').visible();
    //apply animation

    $(".dust").each(function(index) {
        animateBlur($(this), 0.8, 800);
        setTimeout(() => {
            animateTransform($(this), 100, -100, chance.integer({ min: -15, max: 15 }), 800 + (110 * index));
        }, 70 * index);
        //remove the canvas from DOM tree when faded
        $(this).delay(70 * index).fadeOut((110 * index) + 800, "easeInQuint", () => { $(this).remove(); });
    });
}

function weightedRandomDistrib(peak) {
    var prob = [],
        seq = [];
    for (let i = 0; i < canvasCount; i++) {
        prob.push(Math.pow(canvasCount - Math.abs(peak - i), 3));
        seq.push(i);
    }
    return chance.weighted(seq, prob);
}

function animateBlur(elem, radius, duration) {
    var r = 0;
    $({ rad: 0 }).animate({ rad: radius }, {
        duration: duration,
        easing: "easeOutQuad",
        step: function(now) {
            elem.css({
                filter: 'blur(' + now + 'px)'
            });
        }
    });
}

function animateTransform(elem, sx, sy, angle, duration) {
    var td = tx = ty = 0;
    $({ x: 0, y: 0, deg: 0 }).animate({ x: sx, y: sy, deg: angle }, {
        duration: duration,
        easing: "easeInQuad",
        step: function(now, fx) {
            if (fx.prop == "x")
                tx = now;
            else if (fx.prop == "y")
                ty = now;
            else if (fx.prop == "deg")
                td = now;
            elem.css({
                transform: 'rotate(' + td + 'deg)' + 'translate(' + tx + 'px,' + ty + 'px)'
            });
        }
    });
}

function createBlankImageData(imageData, imageDataArray) {
    for (let i = 0; i < canvasCount; i++) {
        let arr = new Uint8ClampedArray(imageData.data);
        for (let j = 0; j < arr.length; j++) {
            arr[j] = 0;
        }
        imageDataArray.push(arr);
    }
}

function newCanvasFromImageData(imageDataArray, w, h) {
    var canvas = document.createElement('canvas');
    canvas.width = w;
    canvas.height = h;
    tempCtx = canvas.getContext("2d");
    tempCtx.putImageData(new ImageData(imageDataArray, w, h), 0, 0);

    return canvas;
}